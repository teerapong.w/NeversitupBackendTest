
const express = require('express')
const router = express.Router()
const verifyToken = require('../middlewares/token')
const getUserData = require('../middlewares/getUserData');
const fs = require('fs');
let jsonUsers = require('../users.json');
let jsonOrder = require('../orders.json');
const jwt = require('jsonwebtoken')
const monk = require('monk');
const url = `mongodb://10.0.1.11:27017/RA_db_test`
const dbMonk = monk(url);

// dbMonk.then(() => {
//      console.log('Connected correctly to server')
// })
const userCollection = dbMonk.get('TestUsers')
const orderCollection = dbMonk.get('TestOrders')

router.get('/', verifyToken, async (req, res, next) => {
  try {
    var token = req.headers.authorization
    // verifyToken.decodeToken(token)
    var tokenDetail = await getUserData.getDataFromToken(token)
    // let dataArray = jsonUsers.dataUsers
    // let dataNum = await dataArray.findIndex(a => a.userEmail == tokenDetail.userEmail)
    // if (dataNum < 0) throw new Error('not found user')
    let data = await userCollection.findOne({userEmail: tokenDetail.userEmail})
    res.status(200).send({
      'success': true,
      'result': data
    })
  } catch (error) {
    console.log(error.message)
    res.status(400).send({
      success: false,
      msg: error
    });
  }
})

router.get('/orderHistory', verifyToken, async (req, res, next) => {
  try {
    var token = req.headers.authorization
    // verifyToken.decodeToken(token)
    var tokenDetail = await getUserData.getDataFromToken(token)
    console.log(tokenDetail);
    let data = await orderCollection.find({userId: tokenDetail._id})
    // let dataArray = jsonOrder.dataOrders
    // let data = await dataArray.filter(a => a.userId == tokenDetail.id)
    // if (dataNum < 0) throw new Error('not found user')
    res.status(200).send({
      'success': true,
      'result': data
    })
  } catch (error) {
    console.log(error.message)
    res.status(400).send({
      success: false,
      msg: error
    });
  }
})

module.exports = router;
