var express = require('express');
var router = express.Router();
const fs = require('fs');
let jsonUsers = require('../users.json');
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const monk = require('monk');
const url = `mongodb://10.0.1.11:27017/RA_db_test`
const dbMonk = monk(url);

dbMonk.then(() => {
     console.log('Connected correctly to server')
})
const userCollection = dbMonk.get('TestUsers')


router.post('/', async (req, res, next) => {
     try {
          const { userName, userPass } = req.body
          // let dataArray = jsonUsers.dataUsers
          // let dataNum = await dataArray.findIndex(a => a.userName == userName)
          // if (dataNum < 0) throw new Error('usernotfound')
          let data = await userCollection.findOne({ userName: userName })
          // console.log(data);
          if (data) {
               const isvalituser = bcrypt.compareSync(userPass, data.userPass)
               if (isvalituser) {
                    delete data.userPass
                    const payload = {
                         user: {
                              userData: data
                         }
                    }
                    const opts = {} // expiresIn ? { expiresIn: 60*2 } : {};
                    jwt.sign(payload, 'test-secretkey', {
                         ...opts
                    }, (error, token) => {
                         if (error) {
                              console.log(error)
                         } else {
                              res.status(200).send({
                                   'success': true,
                                   'result': "loginsuccess",
                                   'token': token
                              })
                         }
                    })
               } else {
                    res.status(401).send({
                         success: false,
                         msg: 'wrongpassword'
                    })
               }
          } else {
               res.status(404).send({
                    uccess: false,
                    msg: 'usernotfound'
               })
          }
     } catch (error) {
          res.status(404).send({
               uccess: false,
               msg: error.message
          })
     }
})


module.exports = router