var express = require('express');
var router = express.Router();
const fs = require('fs');
let jsonUsers = require('../users.json');
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const monk = require('monk');
const url = `mongodb://10.0.1.11:27017/RA_db_test`
const dbMonk = monk(url);

// dbMonk.then(() => {
//   console.log('Connected correctly to server')
// })
const userCollection = dbMonk.get('TestUsers')

router.post('/', async (req, res, next) => {
  try {
    const { userEmail, userName, userTel, userPass } = req.body
    if (!req.body.userEmail) throw new Error('userEmail is Empty')
    if (!req.body.userName) throw new Error('userName is userPass')

    const saltRounds = 10
    const salt = bcrypt.genSaltSync(saltRounds)
    const hash = bcrypt.hashSync(userPass, salt)
    let data = await userCollection.find({ email: userEmail })
    // console.log(data);
    if (data.length <= 0) {
      await userCollection.insert({
        "userEmail": userEmail,
        "userTel": userTel,
        "userName": userName,
        "userPass": hash,
        "createdAt": new Date(),
        "updateAt": new Date(),
      })
    }else {
      throw new Error('duplicate user')
    }

    // let dataArray = jsonUsers.dataUsers
    // let dataNum = await dataArray.findIndex(a => a.userEmail == userEmail)
    // if (dataNum >= 0) throw new Error('duplicate user')

    // let dataCreate = {
    //   "id": jsonUsers.dataUsers.length + 1,
    //   "userEmail": userEmail,
    //   "userTel": userTel,
    //   "userName": userName,
    //   "userPass": hash,
    //   "createdAt": new Date(),
    //   "updateAt": new Date(),
    // }

    // jsonUsers.dataUsers.push(dataCreate)
    // fs.writeFileSync('./users.json', JSON.stringify(jsonUsers));
    res.status(200).send({
      'success': true,
      'result': "Register Success"
    })

  } catch (error) {
    console.log(error.message)
    res.status(400).send({
      success: false,
      msg: error
    });
  }
})


module.exports = router
