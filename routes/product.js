var express = require('express');
var router = express.Router();
const fs = require('fs');
let jsonProduct = require('../products.json');
const verifyToken = require('../middlewares/token')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const monk = require('monk');
const url = `mongodb://10.0.1.11:27017/RA_db_test`
const dbMonk = monk(url);

// dbMonk.then(() => {
//      console.log('Connected correctly to server')
// })
const productCollection = dbMonk.get('TestProducts')

router.get('/all', verifyToken, async (req, res, next) => {
     try {
          let data = await productCollection.find()
          res.status(200).send({
               'success': true,
               'count': data.length,
               'result': data
          })
     } catch (error) {
          console.log(error.message)
          res.status(400).send({
            success: false,
            msg: error
          });
        }
})

router.get('/:prodId', verifyToken, async (req, res, next) => {
     try {
          const prodId = req.params.prodId
          // let dataArray = jsonProduct.dataProducts
          // let dataNum = await dataArray.findIndex(a => a.id == prodId)
          // if (dataNum < 0) throw new Error('not found user')
          let data = await productCollection.findOne({_id: prodId })
          res.status(200).send({
               'success': true,
               'result': data
          })
     } catch (error) {
          console.log(error.message)
          res.status(400).send({
            success: false,
            msg: error
          });
        }
})

router.post('/', async (req, res, next) => {
     try {

          await productCollection.insert({
               "productName": "ไข่",
               "productPrice": 20,
               "productStock": 10,
               "createdAt": new Date(),
               "updateAt": new Date(),
          })

          res.status(200).send({
               'success': true,
               'result': "Success"
          })

     } catch (error) {
          console.log(error.message)
          res.status(400).send({
               success: false,
               msg: error
          });
     }
})

module.exports = router;