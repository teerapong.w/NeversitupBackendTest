var express = require('express');
var router = express.Router();
const fs = require('fs');
let jsonProduct = require('../products.json');
let jsonOrder = require('../orders.json');
const verifyToken = require('../middlewares/token')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const getUserData = require('../middlewares/getUserData');
const monk = require('monk');
const url = `mongodb://10.0.1.11:27017/RA_db_test`
const dbMonk = monk(url);

// dbMonk.then(() => {
//      console.log('Connected correctly to server')
// })
const orderCollection = dbMonk.get('TestOrders')
const productCollection = dbMonk.get('TestProducts')

router.post('/', verifyToken, async (req, res, next) => {
     try {
          const { productId, count } = req.body
          var token = req.headers.authorization
          var tokenDetail = await getUserData.getDataFromToken(token)
          if (!req.body.productId) throw new Error('productId is Empty')
          if (!req.body.count) throw new Error('count is Empty')
          // let dataArray = jsonProduct.dataProducts
          // let dataNum = await dataArray.findIndex(a => a.id == productId)
          // if (dataNum < 0) throw new Error('not found productId')
          let dataProduct = await productCollection.findOne({ _id: productId })

          if (dataProduct.productStock > 0 && dataProduct.productStock > count) {
               await orderCollection.insert({
                    "userId": tokenDetail._id,
                    "productId": productId,
                    "productName": dataProduct.productName,
                    "count": count,
                    "status": 'pending',
                    "createdAt": new Date(),
                    "updateAt": new Date(),
               })

               res.status(200).send({
                    'success': true,
                    'result': 'create order success'
               })
          } else {
               throw new Error('Out of Stock')
          }
     } catch (error) {
          console.log(error.message)
          res.status(400).send({
               success: false,
               msg: error
          });
     }
})

router.get('/:ordId', verifyToken, async (req, res, next) => {
     try {
          const ordId = req.params.ordId
          let data = await orderCollection.findOne({ _id: ordId })
          // let dataArray = jsonOrder.dataOrders
          // let dataNum = await dataArray.findIndex(a => a.id == ordId)
          // if (dataNum < 0) throw new Error('not found ordId')

          res.status(200).send({
               'success': true,
               'result': data
          })
     } catch (error) {
          console.log(error.message)
          res.status(400).send({
               success: false,
               msg: error
          });
     }
})

router.put('/status', verifyToken, async (req, res, next) => {
     try {
          const ordId = req.body.ordId
          const status = req.body.status
          let dataOrder = await orderCollection.findOne({ _id: ordId })
          if (status == 'approve') {
               let dataProduct = await productCollection.findOne({ _id: dbMonk.id(dataOrder.productId)  })
               if (!dataProduct) throw new Error('not found productId')
               if (dataProduct.productStock > dataOrder.count) {
                    // dataArrayP[dataP].productStock = dataArrayP[dataP].productStock - dataArray[dataNum].count
                    // fs.writeFileSync('./products.json', JSON.stringify(jsonProduct));
                    dataOrder.status = 'approve'
                    await productCollection.findOneAndUpdate(
                         { "_id": dbMonk.id(dataOrder.productId) },
                         { $set: { "productStock": dataProduct.productStock - dataOrder.count } }
                    );
               } else {
                    dataOrder.status = 'reject'
               }
          } else {
               dataOrder.status = 'cancel'
          }

          // jsonOrder.dataOrders[dataNum].updateAt = new Date()
          // fs.writeFileSync('./orders.json', JSON.stringify(jsonOrder));
          await orderCollection.findOneAndUpdate(
               { "_id": dbMonk.id(ordId)  },
               { $set: { "status": dataOrder.status } }
          );
          res.status(200).send({
               'success': true,
               'result': "update success"
          })
     } catch (error) {
          console.log(error.message)
          res.status(400).send({
               success: false,
               msg: error
          });
     }
})


module.exports = router;