const fs = require('fs');
let jsonUsers = require('../users.json');
const jwt = require('jsonwebtoken')

const decodeToken = token => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, 'test-secretkey', (error, authInfo) => {
      if (error) {
        reject(error)
      } else {
        resolve(authInfo)
      }
    })
  })
}
module.exports = {
  getDataFromToken:async (token) => {
    try {

      var token_crypto = token.split(' ')
      token_crypto = token_crypto[1]
      let dataUser = await decodeToken(token_crypto)
      return dataUser.user.userData
    } catch (error) {
      console.log('getDataFromToken Controllers error: ' + error.message)
      return 'Not found'
    }
  
  }
}

