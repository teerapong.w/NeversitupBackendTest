const jwt = require('jsonwebtoken')

const verifyToken = (req, res, next) => {
  let accessToken = false
  const bearerHeader = req.headers.authorization
  if (bearerHeader) {
    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]
    req.token = bearerToken
    accessToken = bearerToken
  }

  if (accessToken) {
    decodeToken(accessToken).then(authInfo => {
      res.locals.authInfo = authInfo
      next()
    }).catch(error => {
      console.log(error)
      res.status(401).send({ errormessage: 'invalidtoken' })
    })
  } else {
    res.status(401).send({ errormessage: 'invalidtoken' })
  }
}

const decodeToken = token => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, 'test-secretkey', (error, authInfo) => {
      if (error) {
        reject(error)
      } else {
        resolve(authInfo)
      }
    })
  })
}

module.exports = verifyToken
